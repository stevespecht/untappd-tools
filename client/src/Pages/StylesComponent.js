//Name: Steven Specht
//File: StylesComponent.js
//Date: 04/09/2020
//Purpose: Component containing styles pie chart and table

import React from "react";
import PieChart from "react-minimal-pie-chart";
import { MuiThemeProvider, withStyles } from "@material-ui/core/styles";
import { Card, CardContent, Switch } from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import PieSliceDialog from "./PieSliceDialog";

class StylesComponent extends React.PureComponent {
  state = {
    styleChartData: [],
    styleTableData: [],
    alphaSortStyles: false,
    showPieSliceDialog: false,
    pieSliceDialogText: "",
  };

  componentDidMount() {
    this.loadStyles(this.props.allStyleInfo);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.allStyleInfo !== this.props.allStyleInfo) {
      this.loadStyles(this.props.allStyleInfo);
      this.setState({ alphaSortStyles: false });
    }
  }

  loadStyles = (allStyleInfo) => {
    let dataSet = [];
    let totalBeers = 0;

    for (let i = 0; i < allStyleInfo.length; i++) {
      dataSet.push({
        color: "rgb(255, 204, 0)",
        title: allStyleInfo[i].style,
        value: allStyleInfo[i].beerCount,
      });

      totalBeers += allStyleInfo[i].beerCount;
    }

    for (let i = 0; i < allStyleInfo.length; i++) {
      allStyleInfo[i].percent =
        Math.round(
          ((allStyleInfo[i].beerCount / totalBeers) * 100 + Number.EPSILON) *
            100
        ) /
          100 +
        "%";
    }

    this.setState({
      styleChartData: dataSet,
      styleTableData: allStyleInfo,
    });
  };

  styleSortSwitch = () => {
    this.setState({ alphaSortStyles: !this.state.alphaSortStyles });

    if (this.state.alphaSortStyles) {
      document.getElementById("styleAlphaLable").style.color =
        "rgb(255, 204, 0)";
      document.getElementById("styleBeerLable").style.color =
        "rgba(204, 102, 0, 1)";
      this.setState({
        styleTableData: this.state.styleTableData.sort(function (a, b) {
          return a.style < b.style ? -1 : a.style > b.style ? 1 : 0;
        }),
      });
    } else {
      document.getElementById("styleAlphaLable").style.color =
        "rgba(204, 102, 0, 1)";
      document.getElementById("styleBeerLable").style.color =
        "rgb(255, 204, 0)";
      this.setState({
        styleTableData: this.state.styleTableData.sort(function (a, b) {
          return b.beerCount - a.beerCount;
        }),
      });
    }
  };

  pieSliceClick = (data, dataIndex) => {
    this.setState({
      pieSliceDialogText: "Style: " + data[dataIndex].title,
      showPieSliceDialog: true,
    });
  };

  hideDialog = () => {
    this.setState({ showPieSliceDialog: false });
  };

  render() {
    const {
      styleChartData,
      styleTableData,
      alphaSortStyles,
      showPieSliceDialog,
      pieSliceDialogText,
    } = this.state;

    let styleRows = styleTableData.map((styleData, styleIndex) => {
      return (
        <tr className="tableRow" key={styleIndex}>
          <td className="tableCell">{styleData.style}</td>
          <td className="tableCell secondaryRows">{styleData.beerCount}</td>
          <td className="tableCell secondaryRows">{styleData.percent}</td>
        </tr>
      );
    });

    const StyledSwitch = withStyles({
      switchBase: {
        color: "rgba(30, 145, 234, 1)",
        "&$checked": {
          color: "rgba(30, 145, 234, 1)",
        },
        "&$checked + $bar": {
          backgroundColor: "rgba(30, 145, 234, 1)",
        },
      },
      checked: {
        color: "rgba(30, 145, 234, 1)",
      },
      bar: {
        backgroundColor: "rgba(30, 145, 234, 1)",
      },
    })(Switch);

    return (
      <MuiThemeProvider theme={theme}>
        <div id="BeersPerStyleTitle" className="centerText">
          <h2>Beer Styles</h2>
        </div>
        <Card>
          <CardContent>
            <div className="chartContainer">
              <div className="chartPie">
                <PieChart
                  animate
                  animationDuration={1500}
                  animationEasing="ease-out"
                  cx={50}
                  cy={50}
                  data={styleChartData}
                  label={false}
                  labelPosition={50}
                  labelStyle={{
                    fill: "#121212",
                    fontFamily: "sans-serif",
                    fontSize: "5px",
                  }}
                  lengthAngle={360}
                  lineWidth={100}
                  onClick={(event, data, dataIndex) =>
                    this.pieSliceClick(data, dataIndex)
                  }
                  onMouseOut={undefined}
                  onMouseOver={undefined}
                  paddingAngle={1}
                  radius={25}
                  rounded={false}
                  startAngle={0}
                />
              </div>
              <div className="chartTable">
                <div className="center">
                  <table>
                    <tbody>
                      <tr>
                        <td
                          id="styleAlphaLable"
                          style={{ color: "rgb(255, 204, 0)" }}
                        >
                          Alphabetically
                        </td>
                        <td>
                          <StyledSwitch
                            checked={alphaSortStyles}
                            onChange={this.styleSortSwitch}
                          />
                        </td>
                        <td
                          id="styleBeerLable"
                          style={{ color: "rgba(204, 102, 0, 1)" }}
                        >
                          Beers Per Style
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="center">
                  <div className="tableScrollable">
                    <table className="displayTable">
                      <thead>
                        <tr className="tableRow">
                          <td className="tableHeader tableCell">Style</td>
                          <td className="tableHeader tableCell">
                            Number of Beers
                          </td>
                          <td className="tableHeader tableCell">Percent</td>
                        </tr>
                      </thead>
                      <tbody>{styleRows}</tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </CardContent>
        </Card>
        <PieSliceDialog
          showPieSliceDialog={showPieSliceDialog}
          hideDialog={this.hideDialog}
          pieSliceDialogText={pieSliceDialogText}
        />
      </MuiThemeProvider>
    );
  }
}
export default StylesComponent;
