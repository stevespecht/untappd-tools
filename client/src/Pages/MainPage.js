//Name: Steven Specht
//File: MainPage.js
//Date: 11/05/2019
//Purpose: main page for Uncap Your Stats

import React from "react";
import io from "socket.io-client";
//import ChartistGraph from "react-chartist";
// import PieChart from "react-minimal-pie-chart";
import { MuiThemeProvider, withStyles } from "@material-ui/core/styles";
import {
  Card,
  CardContent,
  FormControl,
  Input,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import Button from "@material-ui/core/Button";
import LogoBar from "./LogoBar";
import CountriesComponent from "./CountriesComponent";
import StylesComponent from "./StylesComponent";

import {
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails,
} from "@material-ui/core/";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

var map,
  breweryMarkers = [];

class Home extends React.PureComponent {
  state = {
    socket: null,
    userName: "",
    disableSearch: true,
    breweries: [],
    filteredBreweries: [],
    url: "",
    showErrorMsg: false,
    errorMsg: "",
    allCountryInfo: {},
    allStyleInfo: {},
    stateToggle: true,
    hideCards: true,
    totalBeers: 0,
    countryFilters: [],
    selectedCountryFilters: [],
    selectedBreweryTableSort: "",
  };

  componentDidMount() {
    //connect to local server
    //const socket = io.connect("localhost:5000", { forceNew: true });
    // connect to server on Heroku
    const socket = io.connect();

    this.setState({ socket: socket }, () => {});
    socket.on("invalidUsername", this.invalidUsername);
    socket.on("breweriesScraped", this.loadBreweries);
    socket.on("countriesScraped", this.setAllCountryInfo);
    socket.on("stylesScraped", this.setAllStyleInfo);
    socket.on("untappdScraped", this.untappdScraped);

    map = new window.google.maps.Map(document.getElementById("map"), {
      zoom: 1,
      center: { lat: 0, lng: 0 },
    });
  }

  //help panel
  demoButtonClick = () => {
    this.state.socket.emit("scrapeUntappd", {
      userName: "",
    });

    this.setState({ userName: "" });
  };

  //search
  handleUserName = (e) => {
    this.setState({ userName: e.target.value });
    if (e.target.value === "") {
      this.setState({
        disableSearch: true,
        showErrorMsg: false,
        errorMsg: "",
      });
    } else {
      this.setState({ disableSearch: false });
    }
  };

  searchButtonClick = (e) => {
    this.sendUsername();
  };

  keyPress = (e) => {
    if (e.keyCode === 13) {
      if (this.state.msg !== "") {
        this.sendUsername();
      }
    }
  };

  sendUsername = () => {
    this.state.socket.emit("scrapeUntappd", {
      userName: this.state.userName,
    });

    this.setState({
      showErrorMsg: false,
      errorMsg: "",
      showDataError: false,
      dataError: "",
    });
  };

  //brewery map
  loadBreweries = (breweries) => {
    if (breweries.length > 0) {
      this.setState(
        {
          breweries: breweries,
          filteredBreweries: breweries,
          showErrorMsg: false,
          errorMsg: "",
          selectedCountryFilters: [],
          selectedBreweryTableSort: "Brewery",
        },
        () => {
          this.addMarkers();
          this.getCountryFilters();
        }
      );
    } else {
      this.setState({
        showErrorMsg: false,
        errorMsg:
          "There aren't any beers associated with this account. Start checking in beers to see your beer stats",
      });
    }
  };

  addMarkers = () => {
    //remove all markers
    for (let i = 0; i < breweryMarkers.length; i++) {
      breweryMarkers[i].setMap(null);
    }

    breweryMarkers.length = 0;

    for (let i = 0; i < this.state.filteredBreweries.length; i++) {
      //create marker
      let marker = new window.google.maps.Marker({
        map: map,
        position: new window.google.maps.LatLng(
          this.state.filteredBreweries[i].latitude,
          this.state.filteredBreweries[i].longitude
        ),
      });

      //create info panel
      let infoText =
        "<b>" +
        this.state.filteredBreweries[i].brewery +
        "</b><br>" +
        this.state.filteredBreweries[i].address +
        "<br><i>Number of beers : " +
        this.state.filteredBreweries[i].beerCount +
        "</i>";

      let infowindow = new window.google.maps.InfoWindow();

      window.google.maps.event.addListener(
        marker,
        "click",
        (function (marker, infoText, infowindow) {
          return function () {
            let openInfoWindow;
            if (openInfoWindow) {
              openInfoWindow.close();
            }

            infowindow.setContent(infoText);
            infowindow.open(map, marker);
            openInfoWindow = infowindow;
          };
        })(marker, infoText, infowindow)
      );

      breweryMarkers.push(marker);
    }
  };

  //handle emit to
  setAllCountryInfo = (allCountryInfo) => {
    this.setState({ allCountryInfo: allCountryInfo });
  };

  setAllStyleInfo = (allStyleInfo) => {
    this.setState({ allStyleInfo: allStyleInfo });
  };

  invalidUsername = () => {
    this.setState({
      showErrorMsg: true,
      errorMsg:
        "This account cannot be found on Untappd.com. Please ensure the username you entered was correct",
      hideCards: true,
    });
  };

  //brewery map country filter
  getCountryFilters = () => {
    this.setState({
      countryFilters: [
        ...new Set(this.state.breweries.map((brewery) => brewery.country)),
      ].sort(),
    });
  };

  countryFilterStyle = (selectedCountry, selectedCountryFilters, theme) => {
    return {
      fontWeight:
        selectedCountryFilters.indexOf(selectedCountry) === -1
          ? theme.typography.fontWeightRegular
          : theme.typography.fontWeightMedium,
    };
  };

  handlerBreweryFilter = (event) => {
    this.setState(
      {
        selectedCountryFilters: event.target.value,
      },
      () => this.filterCountry()
    );
  };

  filterCountry = () => {
    if (this.state.selectedCountryFilters.length > 0) {
      this.setState(
        {
          filteredBreweries: this.state.breweries.filter((brewery) =>
            this.state.selectedCountryFilters.includes(brewery.country)
          ),
        },
        () => this.addMarkers()
      );
    } else {
      this.setState(
        {
          filteredBreweries: this.state.breweries,
        },
        () => this.addMarkers()
      );
    }
  };

  //brewery map sort
  handleBrewerySort = (event) => {
    this.setState({
      selectedBreweryTableSort: event.target.value,
    });
  };

  sortBreweryTable = (a, b) => {
    if (this.state.selectedBreweryTableSort === "Brewery") {
      return a.brewery < b.brewery ? -1 : a.brewery > b.brewery ? 1 : 0;
    } else if (this.state.selectedBreweryTableSort === "Country") {
      return a.country < b.country ? -1 : a.country > b.country ? 1 : 0;
    } else if (this.state.selectedBreweryTableSort === "Number of Beers") {
      return a.beerCount > b.beerCount ? -1 : a.beerCount < b.beerCount ? 1 : 0;
    }
  };

  //card
  untappdScraped = () => {
    let totalBeers = 0;

    this.state.allCountryInfo.forEach(
      (countryInfo) => (totalBeers += countryInfo.beerCount)
    );

    this.setState({ hideCards: false, totalBeers: totalBeers });
    document.getElementById("numberOfBeers").scrollIntoView({
      behavior: "smooth",
      block: "start",
      inline: "nearest",
    });
  };

  getCurrentYear = () => new Date().getFullYear();

  render() {
    const {
      userName,
      disableSearch,
      showErrorMsg,
      errorMsg,
      hideCards,
      totalBeers,
      filteredBreweries,
      allCountryInfo,
      allStyleInfo,
      countryFilters,
      selectedCountryFilters,
      selectedBreweryTableSort,
    } = this.state;

    const StyledExpansionPanelSummary = withStyles({
      content: {
        flexGrow: 0,
        display: "block",
      },
    })(ExpansionPanelSummary);

    let breweryRows = filteredBreweries
      .sort((a, b) => this.sortBreweryTable(a, b))
      .map((breweryData, breweryIndex) => {
        return (
          <tr className="tableRow" key={breweryIndex}>
            <td className="tableCell">{breweryData.brewery}</td>
            <td className="tableCell secondaryRows">{breweryData.address}</td>
            <td className="tableCell secondaryRows">{breweryData.country}</td>
            <td className="tableCell secondaryRows">{breweryData.beerCount}</td>
          </tr>
        );
      });

    let countryFilterValues = countryFilters.map((country) => {
      return (
        <MenuItem key={country} value={country}>
          {country}
        </MenuItem>
      );
    });

    return (
      <MuiThemeProvider theme={theme}>
        <LogoBar logoSize={30} />
        {/* Feature List */}
        <ExpansionPanel
          className="helpExpansionPanel"
          style={{
            boxShadow: "none",
            marginTop: 20,
            color: "#cc6600",
            backgroundColor: "transparent",
          }}
        >
          <StyledExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <div className="centerText">
              What features does Uncap Your Stats offer? Expand for more info
            </div>
          </StyledExpansionPanelSummary>
          <ExpansionPanelDetails
            className="centerText"
            style={{ display: "block", marginTop: -5 }}
          >
            Uncap Your Stats is a fan made companion site to Untappd.com with
            the goal of providing visual statistics to Untappd users
            <br />
            <br />
            Uncap Your Stats breaks down the countries you've had beers from,
            the styles you like, aswell as a map displaying the location of all
            the breweries you've had a beer from around the world
            <br />
            <br />
            Don't have an Untappd account and still want to see the full extent
            of Uncap Your Stats?
            <div />
            <Button
              variant="contained"
              color="secondary"
              onClick={this.demoButtonClick}
              className="center"
              style={{
                marginTop: 15,
                marginBottom: -20,
                textTransform: "none",
                fontFamily: "Josefin Sans",
              }}
            >
              Give me a demo!
            </Button>
          </ExpansionPanelDetails>
        </ExpansionPanel>
        <div className="centerText">
          Enter your Untappd username to see some neat info about your craft
          beer tastes
        </div>
        {/* Search Bar */}
        <Card style={{ marginTop: 20 }}>
          <CardContent>
            <div style={{ textAlign: "center" }}>
              <TextField
                onChange={this.handleUserName}
                placeholder="Untappd Username"
                autoFocus={true}
                required
                value={userName}
                error={showErrorMsg}
                helperText={errorMsg}
                onKeyDown={this.keyPress}
                style={{ width: "90%" }}
              />
            </div>
            <div
              style={{
                textAlign: "center",
                marginTop: "10px",
                marginBottom: "-10px",
              }}
            >
              <Button
                variant="contained"
                color="secondary"
                onClick={this.searchButtonClick}
                disabled={disableSearch}
                style={{ textTransform: "none", fontFamily: "Josefin Sans" }}
              >
                Search
              </Button>
            </div>
          </CardContent>
        </Card>
        {!hideCards && (
          <div id="numberOfBeers">
            <h1 className="totalTitle" style={{ paddingTop: 30 }}>
              You've had
              <span style={{ fontStyle: "italic", color: "rgba(255, 204, 0)" }}>
                &nbsp;{totalBeers}&nbsp;
              </span>
              different beers!
            </h1>
          </div>
        )}
        {!hideCards && <CountriesComponent allCountryInfo={allCountryInfo} />}
        {!hideCards && <StylesComponent allStyleInfo={allStyleInfo} />}
        {/* Brewery Map */}
        <div style={{ display: hideCards ? "none" : "block" }}>
          <div className="centerText">
            <h2>BREWERY MAP</h2>
          </div>
          <div className="centerText" style={{ marginBottom: 10 }}>
            Notice: You may spot some inaccuracies in the location of the
            breweries you've had beers from. There are 2 reasons a brewery may
            have been placed in the wrong location
            <br />
            <br />
            1. If a larger brewery has more than one bottling plant, the marker
            may have been placed at the location of a secondary bottling plant
            instead of the brewery's original location
            <br />
            <br />
            2. There could be multiple breweries with the same name and the
            marker was placed on the wrong one
            <br />
            <br />
            Sorry for the inconvenience
          </div>
          <Card>
            <CardContent>
              {/* map */}
              <div id="map" className="centerMap" />
              {/* controls*/}
              <div className="mapControlsContainer">
                <div className="mapControls">
                  <FormControl style={{ minWidth: 120, maxWidth: "90%" }}>
                    <InputLabel id="mapTableSortDropDown">Sort By</InputLabel>
                    <Select
                      id="mapTableSortDropDown"
                      value={selectedBreweryTableSort}
                      onChange={this.handleBrewerySort}
                    >
                      <MenuItem value={"Brewery"}>Brewery</MenuItem>
                      <MenuItem value={"Country"}>Country</MenuItem>
                      <MenuItem value={"Number of Beers"}>
                        Number of Beers
                      </MenuItem>
                    </Select>
                  </FormControl>
                </div>
                <div className="mapControls">
                  <FormControl style={{ minWidth: 150, maxWidth: "90%" }}>
                    <InputLabel id="mapTableFilterDropDown">
                      Filter By Country
                    </InputLabel>
                    <Select
                      id="mapTableFilterDropDown"
                      multiple
                      value={selectedCountryFilters}
                      onChange={this.handlerBreweryFilter}
                      input={<Input />}
                    >
                      {countryFilterValues}
                    </Select>
                  </FormControl>
                </div>
              </div>
              {/* table */}
              <div className="centerMapTable" style={{ marginTop: 20 }}>
                <div className="tableScrollable">
                  <table className="displayTable">
                    <thead>
                      <tr className="tableRow">
                        <td className="tableHeader tableCell">Brewery</td>
                        <td className="tableHeader tableCell">Address</td>
                        <td className="tableHeader tableCell">Country</td>
                        <td className="tableHeader tableCell">
                          Number of Beers
                        </td>
                      </tr>
                    </thead>
                    <tbody>{breweryRows}</tbody>
                  </table>
                </div>
              </div>
            </CardContent>
          </Card>
        </div>
        {/* footer */}
        <div
          className="centerText"
          style={{
            marginTop: 20,
          }}
        >
          Uncap Your Stats is a fan project and in no way associated with
          Untappd.com
        </div>
        <div
          className="centerText"
          style={{
            marginTop: 10,
          }}
        >
          Steven Specht - © {this.getCurrentYear()}
        </div>
      </MuiThemeProvider>
    );
  }
}
export default Home;
