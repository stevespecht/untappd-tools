import React from "react";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import "../App.css";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function PieSliceDialog(props) {
  const { showPieSliceDialog, pieSliceDialogText, hideDialog } = props;

  return (
    <Dialog
      open={showPieSliceDialog}
      TransitionComponent={Transition}
      keepMounted
      onClose={hideDialog}
      aria-labelledby="alert-dialog-slide-title"
      aria-describedby="alert-dialog-slide-description"
    >
      <DialogTitle id="alert-dialog-slide-title">
        {pieSliceDialogText}
      </DialogTitle>
      <DialogActions>
        <Button onClick={hideDialog} color="primary">
          Ok
        </Button>
      </DialogActions>
    </Dialog>
  );
}
