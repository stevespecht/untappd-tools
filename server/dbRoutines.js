const { Client } = require("pg");

let client;

connectToDB = () => {
  client = new Client({
    user: process.env.PGUSER,
    host: process.env.PGHOST,
    database: process.env.PGDATABASE,
    password: process.env.PGPASSWORD,
    port: process.env.PGPORT,
    //local connection to heroku DB
    // ssl: {
    //   rejectUnauthorized: false
    // }
  });

  client.connect((error) => {
    if (error) {
      console.error("ERROR: connecting to database - ", error.stack);
    } else {
      console.log("Client connected to database");
    }
  });
};

const disconnectFromDB = () => {
  //if client is not undefined and therefore exists then disconnect the client
  if (client) {
    client.end((error) => {
      if (error) {
        console.log("ERROR: disconnecting from database", error.stack);
      } else {
        console.log("Client disconnected from database");
      }
    });
  } else {
    console.log("ERROR: disconnecting from database - client does not exist");
  }
};

const selectBreweries = (breweries) => {
  if (breweries.length > 0) {
    return new Promise((resolve, reject) => {
      try {
        let selectSQL =
          "SELECT brewery, address, country, latitude, longitude FROM breweries WHERE brewery in (";
        let whereSQL = breweries.map(
          (brewery) => `'${escapeSingleQuotes(brewery)}'`
        );

        executeQuery(selectSQL + whereSQL.join() + ")").then((result) => {
          resolve(result);
        });
      } catch (error) {
        reject("Error: selecting breweries - " + error);
      }
    });
  }
};

const insertBreweries = (newBreweries) => {
  if (newBreweries.length > 0) {
    return new Promise((resolve, reject) => {
      try {
        let insertSQL =
          "INSERT INTO breweries (brewery, address, country, latitude, longitude) VALUES ";

        let valuesSQL = newBreweries.map((newBrewery) => {
          return `('${escapeSingleQuotes(
            newBrewery.brewery
          )}', '${escapeSingleQuotes(
            newBrewery.address
          )}', '${escapeSingleQuotes(newBrewery.country)}', ${
            newBrewery.latitude
          }, ${newBrewery.longitude})`;
        });

        executeQuery(insertSQL + valuesSQL.join()).then((result) => {
          resolve(result);
        });
      } catch (error) {
        reject("Error: inserting breweries - " + error);
      }
    });
  }
};

const insertZeroResultBrewery = (newZeroResultBrewery) => {
  return new Promise((resolve, reject) => {
    try {
      let insertSQL = `INSERT INTO zero_result_breweries (brewery)
      SELECT brewery FROM zero_result_breweries
      UNION 
      VALUES ('${escapeSingleQuotes(newZeroResultBrewery)}')
      EXCEPT
      SELECT brewery FROM zero_result_breweries;`;

      executeQuery(insertSQL).then((result) => {
        resolve(result);
      });
    } catch (error) {
      reject("Error: inserting breweries - " + error);
    }
  });
};

const executeQuery = (SQL) => {
  return new Promise((resolve, reject) => {
    try {
      //if client is undefined then connect to database
      if (!client) {
        connectToDB();
      }

      client.query(SQL, (error, result) => {
        if (error) {
          console.error("ERROR: postgres query - " + error.stack);
        } else {
          resolve(result.rows);
        }
      });
    } catch (error) {
      reject("ERROR: executing query - " + error);
    }
  });
};

const escapeSingleQuotes = (text) => {
  if (text.includes("'")) {
    return text.replace(/'/g, "''");
  } else {
    return text;
  }
};

module.exports = {
  connectToDB,
  disconnectFromDB,
  selectBreweries,
  insertBreweries,
  insertZeroResultBrewery,
};
