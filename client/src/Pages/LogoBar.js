//Name: Steven Specht
//File: LogoBar.js
//Date: 11/05/2019
//Purpose: logo bar for untappd tools

import React from "react";
import { MuiThemeProvider } from "@material-ui/core/styles";
import { AppBar, Toolbar } from "@material-ui/core";
import theme from "./theme";
import "../App.css";

class LogoBar extends React.PureComponent {
  state = {
    logoSize: "",
  };

  componentDidMount() {
    this.setState({
      logoSize: this.props.logoSize,
    });
  }

  render() {
    const { logoSize } = this.state;

    return (
      <MuiThemeProvider theme={theme}>
        <AppBar position="static">
          <Toolbar color="primary">
            <div
              style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                width: "100%",
              }}
            >
              <img
                src={"beerIcon.png"}
                alt="beerIcon"
                height={logoSize}
                width={logoSize}
                style={{
                  margin: "0px 0px 7px 0px",
                }}
              />
              <h1> UNCAP YOUR STATS </h1>
            </div>
          </Toolbar>
        </AppBar>
      </MuiThemeProvider>
    );
  }
}
export default LogoBar;
