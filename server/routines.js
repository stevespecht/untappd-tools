//Name: Steven Specht
//File: routines.js
//Date: 11/05/2019
//Purpose: server routines

const request = require("request");
const cheerio = require("cheerio");
const dbRoutines = require("./dbRoutines");
let $scrapedHTML;

const accountExists = (userName) => {
  return new Promise((resolve, reject) => {
    try {
      let url = `https://untappd.com/user/${userName}/beers`;

      request(url, function (error, response, body) {
        if (!error) {
          $scrapedHTML = cheerio.load(body);

          //if there is a element containing the id 'maintenance' on the page
          //request has reached Untappd's 404 'This page cannot be found.' page
          //if #maintenance cannot be found then the username designated a valid Untappd user page
          if ($scrapedHTML("#maintenance").length !== 0) {
            resolve(false);
          } else {
            resolve(true);
          }
        } else {
          resolve(false);
        }
      });
    } catch (error) {
      reject("Error: Can't connect to Untappd - " + error);
    }
  });
};

//breweries
const getBreweries = (socket) => {
  return new Promise((resolve, reject) => {
    try {
      let dropDownBreweries = formatBreweries(
        getDropDownValues("#brewery_picker")
      );

      //create array of brewery names from the array of js objects dropDownBreweries
      let dropDownBreweryNames = dropDownBreweries.map(
        (dropDownBrewery) => dropDownBrewery.brewery
      );

      //select breweries in the DB where brewery name match those in dropDownBreweries
      dbRoutines
        .selectBreweries(dropDownBreweryNames)
        .then((selectedBreweries) => {
          //compare the drop down breweries and the breweries in the DB
          //breweries in the drop down but not in the DB need to be geocoded
          let newBreweriesInfo = filerBreweriesToGeocode(
            dropDownBreweries,
            selectedBreweries
          );

          //if theres new breweries they need to be geocoded and added to the db
          //else the selectedBreweries can be emitted to the client
          if (newBreweriesInfo.length !== 0) {
            Promise.all(
              newBreweriesInfo.map((newBrewery) =>
                geocodeBrewery(newBrewery)
                  .then((geocodedBreweryResults) => geocodedBreweryResults)
                  .catch((error) =>
                    console.log(
                      "ERROR: geocoding new breweries promise - " + error
                    )
                  )
              )
            ).then((geocodedBreweryResults) => {
              let geocodedBreweries = geocodedBreweryResults.filter(
                (breweryResult) => breweryResult !== null
              );

              //insert the geocoded breweries into the DB
              dbRoutines.insertBreweries(geocodedBreweries);

              //add beerCount to selected breweries
              selectedBreweries.forEach((selectedBrewery) => {
                dropDownBreweries.forEach((dropDownBrewery) => {
                  if (selectedBrewery.brewery === dropDownBrewery.brewery) {
                    selectedBrewery.beerCount = dropDownBrewery.beerCount;
                  }
                });
              });

              socket.emit(
                "breweriesScraped",
                selectedBreweries.concat(geocodedBreweries)
              );
            });
          } else {
            //add beerCount to selected breweries
            selectedBreweries.forEach((selectedBrewery) => {
              dropDownBreweries.forEach((dropDownBrewery) => {
                if (selectedBrewery.brewery === dropDownBrewery.brewery) {
                  selectedBrewery.beerCount = dropDownBrewery.beerCount;
                }
              });
            });

            socket.emit("breweriesScraped", selectedBreweries);
          }
        })
        .catch((error) => {
          console.log("ERROR: connecting to database", error.stack);
        });
      resolve();
    } catch (error) {
      reject("Error: getting breweries - " + error);
    }
  });
};

const formatBreweries = (rawBreweries) => {
  let breweries = [];
  let beerCount = [];
  let allBreweryInfo = [];

  breweries = formatDropDownSubject(rawBreweries);
  beerCount = formatBeerCount(rawBreweries);

  //store brewery and beer counts in object array
  for (let i = 0; i < breweries.length; i++) {
    allBreweryInfo.push({
      brewery: breweries[i],
      beerCount: Number(beerCount[i]),
    });
  }

  return allBreweryInfo;
};

const filerBreweriesToGeocode = (dropDownBreweries, selectedBreweries) => {
  let selectedBreweryNames = selectedBreweries.map(({ brewery }) => brewery);

  let newBreweries = dropDownBreweries.filter(
    (dropDownBrewery) => !selectedBreweryNames.includes(dropDownBrewery.brewery)
  );

  return newBreweries;
};

const geocodeBrewery = (newBrewery) => {
  return new Promise((resolve, reject) => {
    try {
      let url =
        "https://maps.googleapis.com/maps/api/geocode/json?address=" +
        encodeURI(newBrewery.brewery) +
        "&key=" +
        process.env.GMAPSKEY;

      request(url, function (error, response, body) {
        if (!error) {
          let parsedBody = JSON.parse(body);

          if (parsedBody.status === "OK") {
            if (parsedBody.results.length > 0) {
              newBrewery.address = parsedBody.results[0].formatted_address;

              parsedBody.results[0].address_components.forEach((address) => {
                if (address.types.includes("country")) {
                  newBrewery.country = address.long_name;
                }
              });

              newBrewery.latitude = parsedBody.results[0].geometry.location.lat;
              newBrewery.longitude =
                parsedBody.results[0].geometry.location.lng;

              return resolve(newBrewery);
            } else {
              console.log(
                "Google Maps Geocoding returned no results for " +
                brewery.brewery
              );
              return resolve(null);
            }
          } else if (parsedBody.status === "ZERO_RESULTS") {
            dbRoutines.insertZeroResultBrewery(newBrewery.brewery);
            return resolve(null);
          } else if (parsedBody.status === "INVALID_REQUEST") {
            console.log(
              "ERROR: Google Maps API HTTP request - INVALID_REQUEST - " + body
            );
            return resolve(null);
          } else {
            console.log(
              "ERROR: Google Maps API HTTP request - UNEXPECTED STATUS - brewery = {" +
              newBrewery.brewery +
              "} - body = " +
              body
            );
            return resolve(null);
          }
        } else {
          console.log("ERROR: Google Maps API HTTP request - " + error);
          return reject(null);
        }
      });
    } catch (error) {
      return reject("Error: Geocoding breweries - " + error);
    }
  });
};

//countries
const getCountries = (socket) => {
  return new Promise((resolve, reject) => {
    try {
      socket.emit(
        "countriesScraped",
        formatCountries(getDropDownValues("#country_picker"))
      );
      resolve();
    } catch (error) {
      reject("Error: getting countries - " + error);
    }
  });
};

const formatCountries = (rawCountries) => {
  let countries = [];
  let beerCount = [];
  let allCountryInfo = [];

  countries = formatDropDownSubject(rawCountries);
  beerCount = formatBeerCount(rawCountries);

  //store countries and beer counts in object array
  for (let i = 0; i < countries.length; i++) {
    allCountryInfo.push({
      country: countries[i],
      beerCount: Number(beerCount[i]),
    });
  }

  return allCountryInfo;
};

//styles
const getStyles = (socket) => {
  return new Promise((resolve, reject) => {
    try {
      socket.emit(
        "stylesScraped",
        formatStyles(getDropDownValues("#style_picker"))
      );
      resolve();
    } catch (error) {
      reject("Error: getting styles - " + error);
    }
  });
};

const formatStyles = (rawStyles) => {
  let styles = [];
  let beerCount = [];
  let allStyleInfo = [];

  styles = formatDropDownSubject(rawStyles);
  beerCount = formatBeerCount(rawStyles);

  //store styles and beer counts in object array
  for (let i = 0; i < styles.length; i++) {
    allStyleInfo.push({
      style: styles[i],
      beerCount: Number(beerCount[i]),
    });
  }

  return allStyleInfo;
};

//drop down
const getDropDownValues = (dropDownId) => {
  let dropDownValues = [];

  $scrapedHTML(dropDownId).filter(function () {
    $scrapedHTML(this)
      .children()
      .each(function (i, e) {
        dropDownValues[i] = $scrapedHTML(this).text();
      });
  });

  //remove the first item, the dropdown term "all"

  dropDownValues.shift();

  return dropDownValues;
};

const formatDropDownSubject = (allRawData) => {
  return allRawData.map((rawData) => {
    let indexOfCount = rawData.lastIndexOf("(");
    if (indexOfCount) {
      return rawData.substring(0, indexOfCount - 1);
    }
  });
};

const formatBeerCount = (allRawData) => {
  return allRawData.map((rawData) => {
    let startOfCountIndex = rawData.lastIndexOf("(");
    let endOfCountIndex = rawData.lastIndexOf(")");
    if (startOfCountIndex && endOfCountIndex) {
      return rawData.substring(startOfCountIndex + 1, endOfCountIndex);
    }
  });
};

module.exports = {
  accountExists,
  getBreweries,
  getCountries,
  getStyles,
};
