//Name: Steven Specht
//File: CountriesComponent.js
//Date: 04/09/2020
//Purpose: Component containing countries pie chart and table

import React from "react";
import PieChart from "react-minimal-pie-chart";
import { MuiThemeProvider, withStyles } from "@material-ui/core/styles";
import { Card, CardContent, Switch } from "@material-ui/core";
import theme from "./theme";
import "../App.css";
import PieSliceDialog from "./PieSliceDialog";

class CountriesComponent extends React.PureComponent {
  state = {
    countryChartData: [],
    countryTableData: [],
    alphaSortCountries: false,
    showPieSliceDialog: false,
    pieSliceDialogText: "",
  };

  componentDidMount() {
    this.loadCountries(this.props.allCountryInfo);
  }

  componentDidUpdate(prevProps) {
    if (prevProps.allCountryInfo !== this.props.allCountryInfo) {
      this.loadCountries(this.props.allCountryInfo);
      this.setState({ alphaSortCountries: false });
    }
  }

  loadCountries = (allCountryInfo) => {
    let dataSet = [];
    let totalBeers = 0;

    for (let i = 0; i < allCountryInfo.length; i++) {
      dataSet.push({
        color: "rgb(255, 204, 0)",
        title: allCountryInfo[i].country,
        value: allCountryInfo[i].beerCount,
      });

      totalBeers += allCountryInfo[i].beerCount;
    }

    for (let i = 0; i < allCountryInfo.length; i++) {
      allCountryInfo[i].percent =
        Math.round(
          ((allCountryInfo[i].beerCount / totalBeers) * 100 + Number.EPSILON) *
            100
        ) /
          100 +
        "%";
    }

    this.setState({
      countryChartData: dataSet,
      countryTableData: allCountryInfo,
    });
  };

  countrySortSwitch = () => {
    this.setState({ alphaSortCountries: !this.state.alphaSortCountries });

    if (this.state.alphaSortCountries) {
      document.getElementById("countryAlphaLable").style.color =
        "rgb(255, 204, 0)";
      document.getElementById("countryBeerLable").style.color =
        "rgba(204, 102, 0, 1)";
      this.setState({
        countryTableData: this.state.countryTableData.sort(function (a, b) {
          return a.country < b.country ? -1 : a.country > b.country ? 1 : 0;
        }),
      });
    } else {
      document.getElementById("countryAlphaLable").style.color =
        "rgba(204, 102, 0, 1)";
      document.getElementById("countryBeerLable").style.color =
        "rgb(255, 204, 0)";
      this.setState({
        countryTableData: this.state.countryTableData.sort(function (a, b) {
          return b.beerCount - a.beerCount;
        }),
      });
    }
  };

  pieSliceClick = (data, dataIndex) => {
    this.setState({
      pieSliceDialogText: "Country: " + data[dataIndex].title,
      showPieSliceDialog: true,
    });
  };

  hideDialog = () => {
    this.setState({ showPieSliceDialog: false });
  };

  render() {
    const {
      countryChartData,
      countryTableData,
      alphaSortCountries,
      showPieSliceDialog,
      pieSliceDialogText,
    } = this.state;

    let countryRows = countryTableData.map((countryData, countryIndex) => {
      return (
        <tr className="tableRow" key={countryIndex}>
          <td className="tableCell">{countryData.country}</td>
          <td className="tableCell secondaryRows">{countryData.beerCount}</td>
          <td className="tableCell secondaryRows">{countryData.percent}</td>
        </tr>
      );
    });

    const StyledSwitch = withStyles({
      switchBase: {
        color: "rgba(30, 145, 234, 1)",
        "&$checked": {
          color: "rgba(30, 145, 234, 1)",
        },
        "&$checked + $bar": {
          backgroundColor: "rgba(30, 145, 234, 1)",
        },
      },
      checked: {
        color: "rgba(30, 145, 234, 1)",
      },
      bar: {
        backgroundColor: "rgba(30, 145, 234, 1)",
      },
    })(Switch);

    return (
      <MuiThemeProvider theme={theme}>
        <div id="BeersPerCountryTitle" className="centerText">
          <h2>Beers Per Country</h2>
        </div>
        <Card>
          <CardContent>
            <div className="chartContainer">
              <div className="chartPie">
                <PieChart
                  animate
                  animationDuration={1500}
                  animationEasing="ease-out"
                  cx={50}
                  cy={50}
                  data={countryChartData}
                  label={false}
                  labelPosition={50}
                  labelStyle={{
                    fill: "#121212",
                    fontFamily: "sans-serif",
                    fontSize: "5px",
                  }}
                  lengthAngle={360}
                  lineWidth={100}
                  onClick={(event, data, dataIndex) =>
                    this.pieSliceClick(data, dataIndex)
                  }
                  onMouseOut={undefined}
                  onMouseOver={undefined}
                  paddingAngle={1}
                  radius={25}
                  rounded={false}
                  startAngle={0}
                />
              </div>
              <div className="chartTable">
                <div className="center">
                  <table>
                    <tbody>
                      <tr>
                        <td
                          id="countryAlphaLable"
                          style={{ color: "rgb(255, 204, 0)" }}
                        >
                          Alphabetically
                        </td>
                        <td>
                          <StyledSwitch
                            checked={alphaSortCountries}
                            onChange={this.countrySortSwitch}
                          />
                        </td>
                        <td
                          id="countryBeerLable"
                          style={{ color: "rgba(204, 102, 0, 1)" }}
                        >
                          Beers Per Country
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div className="center">
                  <div className="tableScrollable">
                    <table className="displayTable">
                      <thead>
                        <tr className="tableRow">
                          <td className="tableHeader tableCell">Country</td>
                          <td className="tableHeader tableCell">
                            Number of Beers
                          </td>
                          <td className="tableHeader tableCell">Percent</td>
                        </tr>
                      </thead>
                      <tbody>{countryRows}</tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </CardContent>
        </Card>
        <PieSliceDialog
          showPieSliceDialog={showPieSliceDialog}
          hideDialog={this.hideDialog}
          pieSliceDialogText={pieSliceDialogText}
        />
      </MuiThemeProvider>
    );
  }
}
export default CountriesComponent;
