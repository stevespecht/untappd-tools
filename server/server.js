//Name: Steven Specht
//File: server.js
//Date: 11/05/2019
//Purpose: socket server

require("dotenv").config();
const express = require("express");
const app = express();
const http = require("http");
const port = process.env.PORT || 5000;
let server = http.createServer(app);
const socketIO = require("socket.io");
let io = socketIO(server);
const routines = require("./routines");
const dbRoutines = require("./dbRoutines");
let username;

app.use(express.static("public"));
app.get("/", (req, res) => res.send("<h1>Untappd Tools Server</h1>"));

io.on("connection", (socket) => {
  socket.on("scrapeUntappd", async (clientData) => {
    //if no username is provided the user has requested a demo
    if (clientData.userName !== "") {
      username = clientData.userName;
    } else {
      username = "testAccount2020";
    }

    socket.join(username); // join room

    dbRoutines.connectToDB();

    //validate username to see if account exists
    if (await routines.accountExists(username)) {
      Promise.all([
        routines.getBreweries(socket),
        routines.getCountries(socket),
        routines.getStyles(socket),
      ]).then(socket.emit("untappdScraped", true));
    } else {
      socket.emit("invalidUsername");
    }
  });

  socket.on("disconnect", async () => {
    dbRoutines.disconnectFromDB();
  });
});

server.listen(port, () => console.log(`Starting on port ${port}`));
