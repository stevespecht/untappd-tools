import React from "react";
import ReactDOM from "react-dom";
import Root from "./Pages/MainPage";
ReactDOM.render(<Root />, document.getElementById("root"));
