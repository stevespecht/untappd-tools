import { createMuiTheme } from "@material-ui/core/styles";
export default createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    common: { black: "#000", white: "#fff" },
    background: { paper: "#fff", default: "#fafafa" },
    primary: {
      light: "rgba(255, 229, 150, 1)",
      main: "rgba(255, 204, 0, 1)",
      dark: "rgba(204, 102, 0, 1)",
      contrastText: "#fff"
    },
    secondary: {
      light: "rgba(131, 202, 255, 1)",
      main: "rgba(30, 145, 234, 1)",
      dark: "rgba(0, 86, 153, 1)",
      contrastText: "#fff"
    },
    error: {
      light: "#e57373",
      main: "#f44336",
      dark: "#d32f2f",
      contrastText: "#fff"
    },
    text: {
      primary: "rgba(0, 0, 0, 0.87)",
      secondary: "rgba(0, 0, 0, 0.54)",
      disabled: "rgba(0, 0, 0, 0.38)",
      hint: "rgba(0, 0, 0, 0.38)"
    }
  }
});
